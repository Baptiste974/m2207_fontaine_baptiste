package td2;

public class Point {
		//Attribut
		public int x,y;
		
		//Constructeurs
		
		public Point(int x1, int y1) {
			x=x1;
			y=y1;
		}
		
		//Accesseurs
		
		public int getX() {
			return x;
		}
		
		public int getY() {
			return y;
		}
		
		public void setX(int x) {
			this.x=x;
		}
		
		public void setY(int y) {
			this.y=y;
		}
		
		//M�thode
		
		//public void afficher() {
		//	System.out.print("Les coordonn�e du point sont " + x + y);
			
		//}
		public void deplacer(int a,int b) {
			x = x + a;
			y = y + b;
		}
}
