package tp1;

public class Client {

	//Attributs
	public String nom;
	public String prenom;
	private Compte compteCourant;
	
	//Constructeur
	public Client(String n,String p,Compte compte) {
		this.nom = n;
		this.prenom = p;
		this.compteCourant = compte;
	}
	
	//Accesseurs
	public String getNom() {
		return this.nom;
	}
	public String getPrenom() {
		return this.prenom;
	}
	public double getSolde() {
		return compteCourant.getSolde();
	}
	
	//M�thodes
	public void afficheSolde() {
		System.out.println("Votre solde est de " + compteCourant.getSolde());
	}
}
