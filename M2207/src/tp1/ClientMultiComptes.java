package tp1;

public class ClientMultiComptes {
	
	//Attributs
	public String nom;
	public String prenom;
	private Compte[] tabcomptes = new Compte[10]; 
	private int nbCompte;
	
	//Constructeurs
	public ClientMultiComptes (String n, String p, Compte compte) {
		tabcomptes[0] = compte;
		this.nbCompte ++;
		
		this.nom=n;
		this.prenom=p;
	}
	
	//Accesseurs
	public void ajouterCompte (Compte c) {
		tabcomptes[nbCompte] = c;
	}
	
	//M�thodes
	public double getSolde() {
		double somme=0;
		for (int i=0;i<=this.nbCompte;i++) {
			
			somme += tabcomptes[i].getSolde();
			
		}
		return somme;
	}
	public void afficheSolde() {
		System.out.println("Votre solde est de " + getSolde());
	}
	
	public void afficheEtatClient() {
		System.out.println("Client : " + this.nom + " " + this.prenom);
		for (int i=0;i<=this.nbCompte;i++) {
			System.out.println("Compte : " + tabcomptes[i].getNumero() + " - Solde : " + tabcomptes[i].getSolde());
		}
		System.out.println("Solde total : " + getSolde());
	}
}
