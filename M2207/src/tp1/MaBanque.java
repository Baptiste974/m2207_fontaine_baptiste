package tp1;


public class MaBanque {

	public static void main(String[] args) {

		//Cr�ation du compte1
		Compte compte1 = new Compte(14521);
		compte1.setDecouvert(100);

		//D�couvert sans modification
		System.out.println("Votre d�couvert est de " + compte1.getDecouvert());

		//R�cup�ration du solde du compte1
		compte1.afficheSolde();

		//Afficher le d�couvert apr�s modification
		System.out.println("Votre d�couvert est de " + compte1.getDecouvert());

		//Ajout d'un d�pot
		compte1.depot(3000);

		//Affiche le nouveau solde
		compte1.afficheSolde();

		//Ajout d'un retrait
		System.out.println(compte1.retrait(3100));
		compte1.afficheSolde();

		//Cr�ation du compte: c2
		Compte c2 = new Compte(2);
		c2.depot(1000);
		c2.afficheSolde();

		System.out.println(c2.retrait(600));
		c2.afficheSolde();

		System.out.println(c2.retrait(700));
		c2.afficheSolde();

		c2.setDecouvert(500);
		System.out.println(c2.retrait(700));
		c2.afficheSolde();

		//Cr�ation client1 qui poss�de le compte 2 (c2)
		Client client1 = new Client("Rivi�re","Fred",c2);
		System.out.println("Le client est " + client1.getNom() + " " + client1.getPrenom());
		client1.afficheSolde();

		//Ajout de soldes de plusieurs compte
		System.out.println("Afficher l'ajout de plusieurs soldes");
		Compte c10 = new Compte(10);
		c10.depot(1000);
		ClientMultiComptes cm10 = new ClientMultiComptes("Barbe", "Bronze", c10);
		System.out.println("Le client est " + cm10.nom + " " + cm10.prenom);
		c10.afficheSolde();
		
		Compte c20 = new Compte(20);
		c20.depot(2500);
		cm10.ajouterCompte(c20);
		cm10.getSolde();
		cm10.afficheSolde();
		
		//Afficher l'�tat des comptes d'un client
		cm10.afficheEtatClient();
		
	}

}
