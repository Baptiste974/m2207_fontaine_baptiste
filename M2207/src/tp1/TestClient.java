package tp1;




public class TestClient {

	public static void main(String[] args) {
		
		//Cr�ation d'un nouveau compte client
		Compte compteClient1 = new Compte(35);
		
		//Affichage du client
		Client client1 = new Client("Payet", "Jean", compteClient1);
		System.out.println("Le client est " + client1.getNom() + " " + client1.getPrenom());
		client1.afficheSolde();
		
		//Cr�ation d'un client multi compte
		Compte comptecm1 = new Compte(3);
		ClientMultiComptes cm1 = new ClientMultiComptes("Jawad", "Alfredo ", comptecm1);
		System.out.println("Le client est " + cm1.nom + " " + cm1.prenom);
	}

}
