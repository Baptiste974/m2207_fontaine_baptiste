package tp1;

public class Compte {

	//Attributs
	private int numero;
	private double decouvert;
	private double solde;
	
	//Constructeurs
	public Compte(int numero) {
		this.numero = numero;
		this.solde = 0;
		this.decouvert = 0;
		
	}
	
	//Accesseurs
	public double getDecouvert() {
		return decouvert;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public double getSolde() {
		return solde;
	}
	
	public void setDecouvert(double montant) {
		this.decouvert = montant;
	}
	
	//M�thodes
	public void afficheSolde() {
		System.out.println("Votre solde est de " + this.solde);
	}
	
	public void depot(double montant) {
		this.solde += montant;
	}
	
	public String retrait(double montant) {
		
		if (this.solde + this.decouvert>= montant) {
			this.solde -= montant;
			return "retrait effectu�";
			
		}else {
			return "retrait refus�";
		}
		
		
	}
}
