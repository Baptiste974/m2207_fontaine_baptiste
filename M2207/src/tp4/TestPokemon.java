package tp4;

import java.util.jar.Pack200;

public class TestPokemon {

	public static void main(String[] args) {
		
		//Cr�ation du premier pok�mon
		Pokemon p1 = new Pokemon("Aligakwak");
		p1.sePresenter();
		
		//Nourrir son pok�mon
		p1.Manger();
		p1.sePresenter();
		
		//Faire vivre le pok�mon
		p1.vivre();
		p1.sePresenter();
		p1.vivre();
		p1.sePresenter();
		p1.vivre();
		p1.sePresenter();
		p1.vivre();
		p1.sePresenter();
		
		//Voir si le pok�mon est vivant
		p1.isAlive();
		System.out.println("Actuellement, " + p1.getNom() + " est " + p1.isAlive());
		
		//Nombre de cycles surv�cu
		Pokemon p2 = new Pokemon("Darturgeon");
		p2.sePresenter();
		p2.cycle();
		
		//Cr�ation de la puissance d'un nouveau pok�mon
		Pokemon p3 = new Pokemon("Cavastirion");
		p3.sePresenter();
		p3.perdreEnergie(5);
		p3.sePresenter();
	}

}
