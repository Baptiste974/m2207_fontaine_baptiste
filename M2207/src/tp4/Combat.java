package tp4;

public class Combat {

	public static void main(String[] args) {
		
		//Pr�sentation des pok�mon avant le combat
		Pokemon p1 = new Pokemon("Spritule");
		Pokemon p2 = new Pokemon("Fantasticot");

		p1.sePresenter();
		p2.sePresenter();
		int c = 0;
		
		//D�but des combats avec gestion de la fatigue
		while(p1.isAlive() == true && p2.isAlive() == true) {
			c++ ;
			p1.attaquer(p2);
			p2.attaquer(p1);
			System.out.println("Round " + c + " " + p1.getNom() + " : " + " (en) " + p1.getEnergie() + " (atk) "+ p1.getPuissance()+ " " + p2.getNom() + " : " + " (en) " + p2.getEnergie() + " (atk) " + p2.getPuissance());
			
		}
		//D�cision d'un vainqueur en fonction de l'�nergie restante
		System.out.println("Le combat s'est d�roul� en " + c + " rounds");
		if (p1.getEnergie()>0) {
			System.out.println("Le vainqueur est " + p1.getNom());
		}else if (p2.getEnergie()>0) {
			System.out.println("Le vainqueur est " + p2.getNom());
		}else {
			System.out.println("Les 2 pok�mons sont KO, il n'y a donc pas de vainqueur");
		}
		
		
	}

}
