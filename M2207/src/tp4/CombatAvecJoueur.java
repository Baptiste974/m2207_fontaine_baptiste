package tp4;

import java.util.Scanner;//biblioth�que pour faire de la saisie clavier

public class CombatAvecJoueur {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		//Attribution d'un nom aux pokemons
		System.out.println("Saisissez le premier pokemon: ");
		Pokemon p1 = new Pokemon(sc.nextLine());

		System.out.println("Saisissez le second pokemon: ");
		Pokemon p2 = new Pokemon(sc.nextLine());

		int c = 0;

		while(p1.isAlive() == true && p2.isAlive() == true) {
			System.out.println("------------------------");
			c++;
			System.out.println("Round " + c);
			System.out.println("Etat des combattants : " + p1.getNom() + " (en) " + p1.getEnergie() + " (atk) " + p1.getPuissance()
			+ " / " + p2.getNom() + " (en) " + p2.getEnergie() + " (atk) " + p2.getPuissance());

			//Choix de l'action en fonction d'un chiffre
			System.out.println(p1.getNom() + " : Attaquer(0) ou Manger(1) (un autre chiffre fais passer votre tour)?" );
			int c1 = sc.nextInt();

			if (c1 == 0) {
				p1.attaquer(p2);
			}else {
				p1.Manger();
			}

			System.out.println(p2.getNom() + " : Attaquer(0) ou Manger(1) (un autre chiffre fais passer votre tour)?" );
			int c2 = sc.nextInt();

			if (c2 == 0) {
				p2.attaquer(p1);
			}else {
				p2.Manger();
			}


		}
		//D�cision d'un vainqueur en fonction de l'�nergie restante
		System.out.println("Le combat s'est d�roul� en " + c + " rounds");
		if (p1.getEnergie()>0) {
			System.out.println("Le vainqueur est " + p1.getNom());
		}else if (p2.getEnergie()>0) {
			System.out.println("Le vainqueur est " + p2.getNom());
		}else {
			System.out.println("Les 2 pok�mons sont KO, il n'y a donc pas de vainqueur");
		}

	}

}
