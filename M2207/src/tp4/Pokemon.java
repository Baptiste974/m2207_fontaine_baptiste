package tp4;

public class Pokemon {
	//attributs
	private int energie;
	private int MaxEnergie;
	private String nom;
	private int puissance;
	
	//accesseurs
	public String getNom() {
		return this.nom;
	}
	
	public int getEnergie() {
		return this.energie;
	}
	
	public int getPuissance() {
		return this.puissance;
	}
	
	//Constructeurs
	public Pokemon(String n) {
		this.nom=n;
		MaxEnergie = 50 + (int)(Math.random() * ((90 - 50) + 1));
		energie = 50 + (int)(Math.random() * ((MaxEnergie - 50) + 1));
		puissance = 3 + (int)(Math.random() * ((10 - 3) + 1));
	}
	
	//M�thodes
	public void sePresenter() {
		System.out.println("Je suis " + getNom() + ", j'ai " + getEnergie() + " point d'�nergie (" + MaxEnergie + " max )");
		System.out.println("J'ai " + getPuissance() + " de puissance");
	}
	
	public void Manger() {
		if (energie > 0) {
			energie = energie + (10 + (int)(Math.random() * ((30 - 10) + 1)));
		}else {
			System.out.println("Je suis trop faible pour manger");
		}
		if (energie > MaxEnergie) {
			energie = MaxEnergie;
		}
	}
	
	public void vivre() {
		energie = energie - (20 + (int)(Math.random() * ((40 - 20) + 1)));
		if (energie < 0) {
			energie = 0;
			System.out.println("Je n'ai plus assez d'�nergie pour vivre");
		}
		
	}
	
	public boolean isAlive() {
		if (energie > 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public void cycle() {
		int c = 0;
		while (energie > 0) {
			Manger();
			vivre();
			c = c+1;
		}
		System.out.println(getNom() + " a surv�cu " + c + " cycles");
	}
	
	public void perdreEnergie(int perte) {
		if (perte > energie) {
			energie = 0;
		}else if (energie <= (MaxEnergie*25)/100){ //Si le pok�mon a moins de 25% de son �nergie
			energie = energie - (int)(perte * 1.5);//il prend 1.5 fois plus de d�gats
		}else {
			energie = energie - perte;
		}
	}
	
	public void attaquer(Pokemon adversaire) {
		if (energie <= (MaxEnergie*25)/100) { //Mode furie si le pok�mon a moins de 25% de son �nergie max
			puissance = puissance * 2;
			adversaire.perdreEnergie(this.getPuissance());
		} else {
			adversaire.perdreEnergie(this.getPuissance());
		}
		puissance = puissance - (0 + (int)(Math.random() * ((1 - 0) + 1)));
		if (puissance <= 0) { //emp�che la puissance de tomber � 0
			puissance = 1;
		}
		
	}

}
