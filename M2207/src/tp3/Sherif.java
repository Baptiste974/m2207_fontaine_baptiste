package tp3;

public class Sherif extends Cowboy{
	//attributs
	public int nbBrigArreter = 0;
	//Construteurs
	public Sherif(String nom) {
		super(nom);
	}
		
	//Accesseurs
	
	//M�thodes
	public void sePresenter() {
		Parler("Sh�rif " + this.nom);
		super.sePresenter();
		Parler("J'ai arr�t� " + this.nbBrigArreter + " brigands");
		
	}
	
	public void Coffrer(Brigand b) {
		b.emprisonner(this);
		this.nbBrigArreter = nbBrigArreter + 1;
		Parler("Au nom de la loi, je vous arr�te, " + b.nom + " !");
	}
}
