package tp3;

public class Cowboy extends Humain{

	//attributs
	private int popularite;
	private String caractéristique;
	
	//Constructeur
	public Cowboy(String nom) {
		super(nom);
		boisson = "whiskey";
		this.popularite = 0;
		this.caractéristique = "vaillant";
	}
	//Accesseurs
	
	//Méthodes
	public void tire(Brigand brigand) {
		System.out.println("Le " + this.caractéristique + this.nom + " tire sur " + brigand.nom + " PAN!");
		Parler("Prends ça voyou!!");
	}
	
	public void libere(Dame dame) {
		dame.estLibre();
		this.popularite = this.popularite + 10;
	}
}
