package tp3;

public class Humain {

	//attributs
	protected String nom;
	protected String boisson;
	
	//Construteurs
	
	public Humain(String nom) {
		this.nom = nom;
		this.boisson = "lait";
	}
	
	//Accesseurs
	
	//M�thodes
	public String quelEstTonNom() {
		return this.nom;
	}
	
	public String quelleEstTaBoisson() {
		return this.boisson;
	}
	
	public void Parler(String texte) {
		System.out.println(this.nom + " - " + texte);
	}
	
	public void sePresenter() {
		this.Parler("Bonjour, je suis " + this.quelEstTonNom() + " et ma boisson pr�f�r�e est le " + this.quelleEstTaBoisson());
	}
	
	public void Boire() {
		this.Parler("Ah! Un bon verre de " + this.boisson + " ! GLOUPS!");
	}
}
