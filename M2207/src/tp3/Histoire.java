package tp3;

public class Histoire {

	public static void main(String[] args) {
		
		//Cr�ation du 1er humain + pr�sentation
		Humain h1 = new Humain("Jean");
		h1.sePresenter();
		h1.Boire();
		
		//Cr�ation d'une Dame + pr�sentation
		Dame d1 = new Dame("Daisy");
		d1.sePresenter();
		d1.Boire();
		d1.priseEnOtage();
		d1.estLibre();
		
		//Cr�ation d'un Brigand + pr�sentation
		Brigand b1 = new Brigand("Gunther");
		b1.sePresenter();
		
		//Cr�ation d'un Cowboy + pr�sentation
		Cowboy c1 = new Cowboy("Simon");
		c1.sePresenter();
		
		//Test d'un enl�vement
		b1.enleve(d1);
		
		//Cr�ation de l'histoire d'un enl�vement
		d1.sePresenter();
		b1.sePresenter();
		b1.enleve(d1);
		b1.sePresenter();
		d1.sePresenter();
		c1.tire(b1);
		d1.sePresenter();
		d1.estLibre();
		
		//Emprisonnement d'un Brigand
		Sherif s1 = new Sherif("Marshall");
		s1.sePresenter();
		s1.Coffrer(b1);
		s1.sePresenter();
		
		//question 9
		//Cowboy clint = new Sherif("clint");
		//clint.sePresenter();
		//clint.Coffrer(b1);
		//clint.sePresenter();
		//Le cowboy ne peut pas coffrer un brigand car c'est d�fini pour le sh�rif et non le cowboy

	}

}
