package tp3;

public class Brigand extends Humain{

	//attributs
	private int nbDame;
	private int prime;
	private boolean prison;
	private String look;
	
	//Constructeur
	public Brigand(String nom) {
		super(nom);
		this.look = "M�chant";
		this.prison = false;
		this.prime = 100;
		this.nbDame = 0;
		boisson = "Cognac";
	}
	
	//Accesseurs
	public int getRecompense() {
		return this.prime;
	}
	
	//M�thodes
	public String quelEstTonNom() {
		return this.nom + " le " + this.look;
	}
	
	public void sePresenter() {
		super.sePresenter();
		Parler("J'ai l'air " + this.look + " et j'ai enlev� " + this.nbDame + " dames");
		Parler("Ma t�te est mise � prix " + this.prime + " $");
	}
	
	public void enleve(Dame dame) {
			dame.priseEnOtage();
			Parler("Ah Ah! " + dame.nom + " tu es ma prisonni�re");
			this.nbDame = this.nbDame + 1 ;
			this.prime = this.prime + 100;
	}
	
	public void emprisonner(Sherif s) {
		Parler("Damned, je suis fait! " + s.nom + ", tu m'as eu !");
	}
}
