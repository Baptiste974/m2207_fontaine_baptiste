package tp3;

public class Dame extends Humain{

	//attributs
	private boolean libre = true;
	
	//Constructeur
	public Dame(String nom) {
		super(nom);
		boisson = "Martini";
	}
	
	//Accesseurs
	
	//M�thodes
	public void priseEnOtage() {
		libre = false;
		Parler("Au secour!!");
	}
	
	public void estLibre() {
		libre = true;
		Parler("Merci cowboy");
	}
	
	public String quelEstTonNom() {
		return "Miss " + this.nom;
	}
	
	public void sePresenter() {
		super.sePresenter();
		if (this.libre==true) {
			Parler("Actuellement je suis libre");
		}else {
			Parler("Actuellement je suis kidnapp�e");
		}
		
	}
}
