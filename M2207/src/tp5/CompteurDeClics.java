package tp5;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.*;

public class CompteurDeClics extends JFrame implements ActionListener{

	//Attribut
	JButton b0;
	JLabel ML1;
	int c = 0;
	
	//Constructeur
	
	public CompteurDeClics() {  
		super();   
		this.setTitle("Ma premi�re application");   
		this.setSize(200,100);   
		this.setLocation(20,20);   
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container panneau = getContentPane();
		b0 = new JButton("Cliquez!!");
		ML1 = new JLabel("Nombre de clique 0");
		panneau.add(b0);
		panneau.add(ML1);
		panneau.setLayout(new FlowLayout());
		b0.addActionListener(this);
		this.setVisible(true);// Toujours � la fin du constructeur
	}
	//Accesseurs

	//M�thodes
	public void actionPerformed(ActionEvent e) {
		c++;
		System.out.println("Une action a �t� d�tect�e");
		ML1.setText("Nombre de clique " + c);
	
	}
	
	
	//M�thode main
	public static void main(String[] args) {
		CompteurDeClics app = new CompteurDeClics();

	}

}
