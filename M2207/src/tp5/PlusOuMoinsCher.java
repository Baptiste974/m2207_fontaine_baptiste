package tp5;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

public class PlusOuMoinsCher extends JFrame implements ActionListener{
	
	//Attributs
	JButton b0;
	JLabel ML1;
	JLabel ML2;
	JTextField TF1;
	int nb;
	
	//Constructeur
	public PlusOuMoinsCher() {
		super();   
		this.setTitle("Plus ou moins chers");   
		this.setSize(350,150);   
		this.setLocation(300,150);   
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container panneau = getContentPane();
		b0 = new JButton("V�rifier");
		ML1 = new JLabel("Votre proposition");
		TF1 = new JTextField("");
		ML2 = new JLabel("R�ponse");
		panneau.add(ML1);
		panneau.add(TF1);
		panneau.add(b0);
		panneau.add(ML2);
		panneau.setLayout(new FlowLayout());
		b0.addActionListener(this);
		panneau.setLayout(new GridLayout(2,2));
		this.setVisible(true);// Toujours � la fin du constructeur
		nb= 0 + (int)(Math.random() * ((100 - 0)));
	}
	//Accesseur
	
	//M�thodes
	public void actionPerformed(ActionEvent e) {
		System.out.println(nb);
		int c = Integer.parseInt(TF1.getText());
		
		if (nb>c) {
			ML2.setText("Plus cher");
		}else if(nb<c){
			ML2.setText("Moins cher");
		}else {
			ML2.setText("Bravo vous avez devin�");
		}
		
		
	}
	
	
	//M�thode main
	public static void main(String[] args) {
		PlusOuMoinsCher app = new PlusOuMoinsCher();

	}
	

}
