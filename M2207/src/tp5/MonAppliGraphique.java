package tp5;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;


public class MonAppliGraphique extends JFrame{
	//Attributs
	JButton b0;
	JButton b1;
	JButton b2;
	JButton b3;
	JButton b4;
	JButton b5;
	JLabel ML1;
	JTextField TF1;
	
	//Constructeurs
	public MonAppliGraphique () {   
		super();   
		this.setTitle("Ma premi�re application");   
		this.setSize(400,200);   
		this.setLocation(20,20);   
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.print("La fen�tre est cr��e !!");
		b0 = new JButton("Boutton 0");
		b1 = new JButton("Boutton 1");
		b2 = new JButton("Boutton 2");
		b3 = new JButton("Boutton 3");
		b4 = new JButton("Boutton 4");
		b5 = new JButton("Boutton 5");
		Container panneau = getContentPane();
		//panneau.add(b1, BorderLayout.NORTH);
		//panneau.add(b2,BorderLayout.SOUTH);
		//panneau.add(b3, BorderLayout.EAST);
		//panneau.add(b4, BorderLayout.WEST);
		//panneau.add(b5);
		panneau.add(b0);
		panneau.add(b1);
		panneau.add(b2);
		panneau.add(b3);
		panneau.add(b4);
		panneau.add(b5);
		ML1 = new JLabel("Je suis un JLabel");
		TF1 = new JTextField("Tapez le texte ici");
		//panneau.add(ML1);
		//panneau.add(TF1);
		//panneau.setLayout(new FlowLayout()); //Le bouton change de taille et ne fait pas la taille de la fen�tre
		panneau.setLayout(new GridLayout(3,2));
		this.setVisible(true);// Toujours � la fin du constructeur
	}
	
	
	//Accesseurs
	
	//M�thodes
	
	//M�thode main
	
	public static void main(String[] arg) {
		MonAppliGraphique app = new MonAppliGraphique();
	}
}
