package tp6;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

public class PanneauGraphique extends JFrame implements ActionListener{
	//attribut
	JTextArea TF1;
	JButton b0;
	JScrollPane scroll;

	//constructeur
	public PanneauGraphique() {
		super();   
		this.setTitle("Serveur~Panneau d'affichage");   
		this.setSize(400,300);   
		this.setLocation(300,150);   
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container panneau = getContentPane();
		TF1 = new JTextArea();
		b0 = new JButton("Exit");
		scroll = new JScrollPane(TF1);
		TF1.append("Serveur actif");
		panneau.add(b0, BorderLayout.SOUTH);
		panneau.add(scroll,BorderLayout.CENTER);
		b0.addActionListener(this);
		this.setVisible(true);// Toujours � la fin du constructeur
	}
	//accesseur

	//M�thode
	public void actionPerformed(ActionEvent e) {
		System.exit(-1);
	}

	//M�thode main
	public static void main(String[] args) {
		PanneauGraphique app = new PanneauGraphique();


	}

}
