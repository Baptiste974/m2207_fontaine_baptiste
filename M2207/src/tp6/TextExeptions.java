package tp6;

public class TextExeptions {

	//public static void main(String[] args) {
	//int x = 2, y = 0;
	//System.out.println(x/y);
	//System.out.println("Fin du programme");
	//On ne peut pas diviser un nombre par 0
	//}
	public static void main(String[] args) {
		int x = 2, y = 0;
		try{
			System.out.println("y/x = " + y/x);
			System.out.println("x/y = " + x/y);
			System.out.println("Commande de fermeture du programme");

		}
		catch (Exception e){
			System.out.println("Une exception a �t� captur�e");
		}
		System.out.println("Fin du programme");
		//L'erreur dans le programme n'arr�te pas le programme
		//L'erreur de la division par 0 a �t� captur�
		
		
	}

}
