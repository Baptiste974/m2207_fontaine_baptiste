package td3;

public class TestAnimal {

	public static void main(String[] args) {
		
		Animal animal = new Animal();
		System.out.println(animal.toString());
		System.out.println(animal.affiche());
		System.out.println(animal.cri());
		System.out.println(animal.origine());
		
		//Exercice 2
		Chien chien = new Chien();
		System.out.println(chien.affiche());
		System.out.println(chien.cri());
		System.out.println(chien.origine());
		
		//Exercice 3
		Chat chat = new Chat();
		System.out.println(chat.affiche());
		System.out.println(chat.miauler());
		
		System.out.println(chat.cri()); //Reprend la m�thod cri dans animal
		//System.out.println(animal.miauler()); miauler n'est pas d�finit dans Animal donc erreur
	}
}
