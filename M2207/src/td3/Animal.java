package td3;

//Exercice 1
public class Animal {

	public String affiche() {
		
		return "Je suis malade";
	}
	
	public String cri() {    //Changer de public � private ne permet pas l'acc�s � cette m�thode
		
		return "...";
	}
	
	 public final String origine() { //Pas besoin de l'ajouter dans Chien car Chien h�rite des m�thodes
		 
		 return "La classe Animal est la m�re de toutes les classes!";
	 }
}
