package td3;

//Exercice 2
public class Chien extends Animal {

	public String affiche() {
		
		return "Je suis un chien";
	}
	
	public String cri() {
		
		return "Ouaf! Ouaf!";
	}
}
