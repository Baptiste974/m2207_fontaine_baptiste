package tp2;

import javax.swing.text.AbstractDocument.LeafElement;

public class TestForme {

	public static void main(String[] args) {
		
		Forme f1 = new Forme();
		Forme f2 = new Forme("vert",false);
		
		//Affichage des valeurs contenu dans f1 et f2
		System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println("f2 : " + f2.getCouleur() + " - " + f2.isColoriage());
		
		//Modification de f1
		f1.setCouleur("rouge");
		f1.setColoriage(false);
		System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		
		//Affichage des information sur f1 et f2
		f1.seDecrire();
		f2.seDecrire();
		System.out.println(f1.seDecrire());
		System.out.println(f2.seDecrire());
		
		
		//Cr�ation d'un cercle c1
		Cercle c1 = new Cercle();
		System.out.println(c1.seDecrire());
		
		//Cr�ation d'un cercle c2
		Cercle c2 = new Cercle(2.5);
		c2.afficheCercle();
		
		//Cr�ation d'un cercle c3
		Cercle c3 = new Cercle(3.2, "Jaune", false);
		System.out.println(c3.seDecrire());
		
		//Calculer et afficher les aires et p�rim�tre de c2 et c3
		System.out.println("Le p�rim�te de c2 est " + c2.calculerPerimetre() + " et d'aire " + c2.calculerAire());
		System.out.println("Le p�rim�te de c3 est " + c3.calculerPerimetre() + " et d'aire " + c3.calculerAire());
		
		//Cr�ation d'un cylindre cy1
		Cylindre cy1 = new Cylindre();
		System.out.println(cy1.seDecrire());
		
		//Cr�ation d'un cylindre cy2
		Cylindre cy2 = new Cylindre(4.2, 1.3, "bleu", true);
		System.out.println(cy2.seDecrire());
		
		//Calcule du volume de cy1 et cy2
		System.out.println("Le volume de cy1 est " + cy1.calculerVolume());
		System.out.println("Le volume de cy2 est " + cy2.calculerVolume());
	}

}
