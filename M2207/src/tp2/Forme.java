package tp2;

public class Forme {
	
	//Attribut
	
	String couleur;
	boolean coloriage;
	
	//Constructeur
	
	public Forme() {
		this.couleur = "orange";
		this.coloriage = true;
	}
	
	public Forme(String c,boolean r) {
	
		this.couleur=c;
		this.coloriage=r;
	}
	
	//Accesseur
	
	public String getCouleur() {
		return this.couleur;
	}
	
	public void setCouleur(String c) {
		this.couleur = c;
	}
	
	public boolean isColoriage() {
		return this.coloriage;
	}
	
	public void setColoriage(boolean b) {
		this.coloriage = b;
	}
	
	//M�thode
	public String seDecrire() {
		return  "Une Forme de couleur " + this.couleur + " et de coloriage " + this.coloriage;
	}
}
