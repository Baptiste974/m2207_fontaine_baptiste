package tp2;

public class Cercle extends Forme{

	//Attribut
	
	private double rayon;
	
	//Constructeur
	
	public Cercle() {
		super();
		this.rayon = 1.0;
	}
	
	public Cercle(double r) {
		this.rayon = r;
	}
	
	public Cercle(double r, String couleur,boolean coloriage) {
		super(couleur,coloriage);
		this.rayon = r;
		
	}
	
	
	//Accesseurs
	
	public double getRayon() {
		return this.rayon;
	}
	
	public void setRayon(double r) {
		this.rayon = r;
	}
	
	//M�thode
	
	public String seDecrire() {
		return "Un Cercle de " + this.rayon + " est issue d�une Forme de couleur " + getCouleur() + " et de coloriage " + isColoriage();
	}
	
	public void afficheCercle() {
		System.out.println("Le rayon de ce cercle est de " + getRayon());
	}
	
	public double calculerAire() {
		double a ;
		return a = Math.PI*(rayon*rayon);
	}
	
	public double calculerPerimetre() {
		double p;
		return p = 2*Math.PI*this.rayon;
	}
}
