package tp2;

public class Cylindre extends Cercle {

	//Attributs
	double hauteur = 1.0;
	
	//Constructeurs
	public Cylindre() {
		super();
	}
	
	public Cylindre(double h,double r,String couleur, boolean coloriage) {
		super(r,couleur,coloriage);
		this.hauteur = r;
	}
	
	//Accesseurs
	public double getHauteur() {
		return this.hauteur;
	}
	
	public void setHauteur(double h) {
		this.hauteur = h;
	}
	
	//M�thodes
	public String seDecrire() {
		return "Un Cylindre de rayon " + getRayon() + super.seDecrire();
	}
	
	public double calculerVolume() {
		double v;
		return Math.PI*(getRayon()*getRayon())*this.hauteur;
	}
}
